<?php


namespace App\Helpers;


use Carbon\Carbon;

trait Time
{
    // 将日期转换为年月 XX年XX月
    public static function toYearAndMonthCN($dateTime)
    {
        $date = date('Y-m', strtotime($dateTime));
        return str_replace('-', '年', $date). '月';
    }
}
