<?php


namespace App\Services;


use App\Models\BlogArticle;
use App\Models\BlogArticleComment;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class CommentService extends BaseService
{
    /**
     * 获取文章下的评论列表
     * @param int $articleId
     * @return BlogArticleComment[]|Builder[]|Collection
     */
    public function list(int $articleId)
    {
        return BlogArticleComment::query()->where('article_id', $articleId)
            ->orderByDesc('id')
            ->get();
    }


    /**
     * 保存评论
     * @param int $articleId
     * @param string $name
     * @param string $email
     * @param string $content
     * @return bool
     */
    public function store(int $articleId, string $name, string $email, string $content)
    {
        $comment = new BlogArticleComment;
        $comment->article_id = $articleId;
        $comment->name = $name;
        $comment->email = $email;
        $comment->content = $content;
        if ($comment->save()){
            BlogArticle::query()->increment('comment_num');
            return true;
        }
        return false;
    }


}
