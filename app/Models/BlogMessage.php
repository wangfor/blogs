<?php

namespace App\Models;


/**
 * App\Models\BlogMessage
 *
 * @property int $id
 * @property string|null $name 姓名
 * @property string|null $email 邮箱
 * @property string|null $content 内容
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMessage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMessage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMessage query()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMessage whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMessage whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMessage whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMessage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMessage whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMessage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BlogMessage extends BaseModel
{

}
