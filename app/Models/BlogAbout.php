<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\BlogAbout
 *
 * @property string $content 内容
 * @method static \Illuminate\Database\Eloquent\Builder|BlogAbout newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogAbout newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogAbout query()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogAbout whereContent($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|BlogAbout whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogAbout whereUpdatedAt($value)
 */
class BlogAbout extends BaseModel
{

}
