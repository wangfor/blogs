<?php

namespace App\Models;


use App\Helpers\Time;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\Article
 *
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticle query()
 * @mixin \Eloquent
 * @property int $id
 * @property string|null $title 标题
 * @property string|null $author 作者
 * @property string|null $brief 摘要
 * @property string|null $content 内容
 * @property int $coment_num 评论数量
 * @property int $read_num 阅读数量
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticle whereAuthor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticle whereBrief($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticle whereComentNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticle whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticle whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticle whereReadNum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticle whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticle whereUpdatedAt($value)
 * @property int $comment_num 评论数量
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticle whereCommentNum($value)
 * @property-read mixed $date
 * @property-read mixed $date_cn
 * @property int|null $category_id 分类ID
 * @property-read \App\Models\BlogCategory|null $category
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticle whereCategoryId($value)
 * @property-read mixed $real_read_num
 */
class BlogArticle extends BaseModel
{
    protected $appends = [
        'date',
        'real_read_num',
    ];

    // 获取真实阅读量
    public function getRealReadNumAttribute()
    {
        return visits(__CLASS__, $this->id)->count();
    }

    public function getDateAttribute()
    {
        return substr($this->created_at, 0, 7);
    }

    public function category()
    {
        return $this->hasOne(BlogCategory::class, 'id', 'category_id');
    }
}
