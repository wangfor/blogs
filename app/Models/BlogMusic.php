<?php

namespace App\Models;


/**
 * App\Models\BlogMusic
 *
 * @property int $id
 * @property string|null $name 歌曲
 * @property string|null $singer 歌手
 * @property string|null $cover_image 封面图
 * @property string|null $url 链接
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMusic newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMusic newQuery()
 * @method static \Illuminate\Database\Query\Builder|BlogMusic onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMusic query()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMusic whereCoverImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMusic whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMusic whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMusic whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMusic whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMusic whereSinger($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMusic whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogMusic whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|BlogMusic withTrashed()
 * @method static \Illuminate\Database\Query\Builder|BlogMusic withoutTrashed()
 * @mixin \Eloquent
 * @property-read string $new_cover_image
 * @property-read string $new_url
 */
class BlogMusic extends BaseModel
{
    protected $table = 'blog_musics';

    protected $appends = [
        'new_cover_image',
        'new_url',
    ];

    public function getNewCoverImageAttribute(): string
    {
        return config('app.url').'/storage/'.$this->attributes['cover_image'];
    }

    public function getNewUrlAttribute(): string
    {
        return config('app.url').'/storage/'.$this->attributes['url'];
    }
}
