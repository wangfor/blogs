<?php

namespace App\Models;


use Carbon\Carbon;

/**
 * App\Models\BlogLife
 *
 * @property int $id
 * @property string|null $content 内容
 * @property mixed|null $images 图片
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|BlogLife newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogLife newQuery()
 * @method static \Illuminate\Database\Query\Builder|BlogLife onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogLife query()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogLife whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogLife whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogLife whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogLife whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogLife whereImages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogLife whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|BlogLife withTrashed()
 * @method static \Illuminate\Database\Query\Builder|BlogLife withoutTrashed()
 * @mixin \Eloquent
 */
class BlogLife extends BaseModel
{
    protected $table = 'blog_lives';

    protected $appends = [
        'date',
    ];

    /**
     * @throws \JsonException
     */
    public function getImagesAttribute()
    {
        $images = $this->attributes['images'];
        if (is_string($images)){
            $imageArr = json_decode($images, true, 512, JSON_THROW_ON_ERROR);
            foreach ($imageArr as $key => $value) {
                $imageArr[$key] = config('app.url').'/storage/'. $value;
            }
            return $imageArr;
        }
        return [];
    }

    public function getDateAttribute()
    {
        return Carbon::parse(strtotime($this->attributes['created_at']))->toDateTimeString();
    }

}
