<?php

namespace App\Models;


/**
 * App\Models\BlogArticleComments
 *
 * @property int $id
 * @property int $article_id 文章ID
 * @property string|null $name 姓名
 * @property string|null $email 邮箱
 * @property string|null $content 内容
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticleComment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticleComment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticleComment query()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticleComment whereArticleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticleComment whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticleComment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticleComment whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticleComment whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticleComment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticleComment whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogArticleComment whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \App\Models\BlogArticle|null $article
 */
class BlogArticleComment extends BaseModel
{
    public function article()
    {
        return $this->hasOne(BlogArticle::class, 'id', 'article_id');
    }
}
