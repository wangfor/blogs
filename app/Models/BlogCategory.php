<?php

namespace App\Models;

/**
 * App\Models\BlogCategory
 *
 * @property int $id
 * @property string|null $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $describe
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\BlogArticle[] $article
 * @property-read int|null $article_count
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory whereDescribe($value)
 * @property int|null $sort
 * @property int|null $status 状态:1=显示,0=隐藏
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlogCategory whereStatus($value)
 */
class BlogCategory extends BaseModel
{
    public function article()
    {
        return $this->hasMany(BlogArticle::class, 'category_id');
    }
}
