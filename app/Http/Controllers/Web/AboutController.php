<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\BlogAbout;
use App\Services\ArticleService;

class AboutController extends Controller
{
    public function index()
    {
        $about = BlogAbout::first();
        $musics = ArticleService::getInstance()->getMusicList();
        return view('about', [
            'about'   => $about['content'],
            'musics'  => $musics,
        ]);
    }
}
