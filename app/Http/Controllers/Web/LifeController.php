<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\BlogLife;
use App\Services\ArticleService;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;


class LifeController extends Controller
{

    public function index(Request $request)
    {
        $musics = ArticleService::getInstance()->getMusicList();
        $avatar = DB::table('admin_users')->first()->avatar;
        $page = $request->input('page') ?? 1;
        $limit = 10;
        // 动态分页数据
        $lives  = BlogLife::query()->orderByDesc('id')->paginate($limit, ['*'], 'page', $page);
        return view('life', [
            'avatar'  => config('app.url').'/storage/'.$avatar,
            'lives'   => $this->successPaginate($lives),
            'musics'  => $musics,
        ]);
    }

}
