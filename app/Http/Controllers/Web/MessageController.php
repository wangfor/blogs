<?php


namespace App\Http\Controllers\Web;


use App\Http\Controllers\Controller;
use App\Models\BlogMessage;
use App\Services\ArticleService;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index()
    {
        $musics = ArticleService::getInstance()->getMusicList();
        return view('message', [
            'musics'  => $musics,
        ]);
    }

    public function store(Request $request)
    {
        $params = $request->all(['name', 'email', 'content']);
        $message = new BlogMessage();
        $message->name = $params['name'];
        $message->email = $params['email'];
        $message->content = $params['content'];
        if ($message->save()){
            return back()->with('success','留言成功');
        }
        return back()->with('error', '留言失败');
    }
}
