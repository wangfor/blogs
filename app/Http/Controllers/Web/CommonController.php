<?php


namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    /**
     * 后台富文本编辑器上传图片
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function adminUploadImages(Request $request)
    {
        $urls = [];
        $rule = ['jpg', 'png'];
        foreach ($request->file() as $file) {
            $clientName = $file->getClientOriginalName();
            $entension = $file->getClientOriginalExtension();
            if (!in_array($entension, $rule)) {
                return $this->fail('图片仅支持jpg,png格式！');
            }
            $newName = md5(date("Y-m-d H:i:s") . $clientName) . "." . $entension;
            $url['url'] = env('app_url').'/storage/images/'.date('Ymd').'/'.$file->store('', 'editor');
//            $manager = new ImageManager(array('driver' => 'imagick'));
//            $image = $manager->make($file)->save('uploads/images/'.date('Ymd').'/aaa.jpg');
//            dd($image);
            $url['alt'] = '';
            $url['href'] = '';
            array_push($urls, $url);
        }
        return [
            "errno" => 0,
            "data"  => $urls,
        ];
    }
}
