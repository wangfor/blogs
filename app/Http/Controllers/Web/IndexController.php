<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\BlogCategory;
use App\Services\ArticleService;
use App\Services\CommentService;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    /**
     * 首页数据
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $where = [];
        $categoryDetail = [];
        $categoryId = $request->input('category_id');
        if (!is_null($categoryId)){
            $categoryDetail = BlogCategory::query()->find($categoryId);
            $where[] = ['category_id', $categoryId];
        }
        $keyword = $request->input('keyword');
        if (!is_null($keyword)) {
            $where[] = ['content', 'like', "$keyword%", 'or'];
            $where[] = ['title', 'like', "%$keyword%", 'or'];
        }
        $date = $request->input('date');
        if (!is_null($date)){
            $where[] = ['created_at', '>=', Carbon::parse($date)->startOfMonth()->toDateString()];
            $where[] = ['created_at', '<=', Carbon::parse($date)->endOfMonth()->toDateString()];
        }
        $page = $request->input('page') ?? 1;
        $limit = 8;
        // 文章列表
        $articles = ArticleService::getInstance()->getArticleList($page, $limit, $where);
        // 右侧栏
        $sidebarData = ArticleService::getInstance()->getSidebarData();
        return view('index', [
            'articles'      => $articles,
            'categoryId'    => $categoryId ?? null,
            'lastArticles'  => $sidebarData['lastArticles'],
            'archiveDates'  => $sidebarData['archiveDates'],
            'categories'    => $sidebarData['categories'],
            'categoryDetail' => $categoryDetail,
        ]);
    }


    /**
     * 详情
     * @param Request $request
     * @return Application|Factory|View
     */
    public function show(Request $request)
    {
        $articleId = $request->route('id');
        $article = ArticleService::getInstance()->getArticleDetail($articleId);
        $comments = CommentService::getInstance()->list($articleId);
        $musics = ArticleService::getInstance()->getMusicList();
        // 右侧栏
        $sidebarData = ArticleService::getInstance()->getSidebarData();
        return view('article', [
            'article'       => $article,
            'musics'        => $musics,
            'comments'      => $comments,
            'lastArticles'  => $sidebarData['lastArticles'],
            'archiveDates'  => $sidebarData['archiveDates'],
            'categories'    => $sidebarData['categories'],
        ]);
    }

    /**
     * 提交评论
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $params = $request->all(['article_id', 'name', 'email', 'content']);
        $store = CommentService::getInstance()->store($params['article_id'], $params['name'], $params['email'], $params['content']);
        if ($store){
            return back()->with('success','评论成功');
        }
        return back()->with('fail','评论失败');
    }

}
