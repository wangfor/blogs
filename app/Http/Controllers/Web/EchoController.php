<?php


namespace App\Http\Controllers\Web;


use App\Http\Controllers\Controller;
use App\Services\ArticleService;
use App\Events\PublicMessageEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class EchoController extends Controller
{
    public function index(Request $request)
    {
        // 聊天记录
//        $lists = Redis::lRange('user_chat_record', -10, -1);
        $musics = ArticleService::getInstance()->getMusicList();
        return view('echo', [
            'ip'    => $request->ip(),
            'musics'  => $musics,
        ]);
    }

    public function store(Request $request)
    {
        $message = $request->input('message');
        $data = [
            'ip' => $request->ip(),
            'message' => $message,
            'time' => now()->toDateTimeString(),
        ];
        $value = json_encode($data);
        Redis::rpush('user_chat_record', $value);
        return response()->json(broadcast(new PublicMessageEvent($message)));
    }
}
