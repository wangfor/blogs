<?php

namespace App\Admin\Repositories;

use App\Models\BlogMessage as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class BlogMessage extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
