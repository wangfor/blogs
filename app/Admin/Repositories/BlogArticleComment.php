<?php

namespace App\Admin\Repositories;

use App\Models\BlogArticleComment as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class BlogArticleComment extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
