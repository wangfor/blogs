<?php

namespace App\Admin\Repositories;

use App\Models\BlogArticle as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class BlogArticle extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;

    public function getGridColumns()
    {
        return [$this->getKeyName(), 'category_id', 'title', 'brief', 'comment_num' , 'read_num', 'created_at'];
    }
}
