<?php

namespace App\Admin\Repositories;

use App\Models\BlogAbout as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class BlogAbout extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
