<?php

namespace App\Admin\Repositories;

use App\Models\BlogLife as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class BlogLife extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
