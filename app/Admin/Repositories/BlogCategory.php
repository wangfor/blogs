<?php

namespace App\Admin\Repositories;

use App\Models\BlogCategory as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class BlogCategory extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
