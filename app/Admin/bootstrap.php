<?php

use Dcat\Admin\Admin;
use Dcat\Admin\Grid;
use Dcat\Admin\Form;
use Dcat\Admin\Grid\Filter;
use Dcat\Admin\Show;

/**
 * Dcat-admin - admin builder based on Laravel.
 * @author jqh <https://github.com/jqhph>
 *
 * Bootstraper for Admin.
 *
 * Here you can remove builtin form field:
 *
 * extend custom field:
 * Dcat\Admin\Form::extend('php', PHPEditor::class);
 * Dcat\Admin\Grid\Column::extend('php', PHPEditor::class);
 * Dcat\Admin\Grid\Filter::extend('php', PHPEditor::class);
 *
 * Or require js and css assets:
 * Admin::css('/packages/prettydocs/css/styles.css');
 * Admin::js('/packages/prettydocs/js/main.js');
 *
 */

// 全局数据表格倒序
Grid::resolving(function (Grid $grid) {
//    $grid->model()->orderBy('id', 'desc');
//    $grid->addTableClass(['table-text-center']);
});

// 注册前端组件别名
Admin::asset()->alias('@wang-editor', [
    'js' => ['/vendor/wangeditor-4.7.11/dist/wangEditor.min.js'],
]);
//Form::extend('editor', \App\Admin\Extensions\Form\WangEditor::class);

