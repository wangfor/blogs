<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\BlogCategory;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class BlogCategoryController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new BlogCategory(), function (Grid $grid) {
            $grid->model()->orderBy('sort');
//            $grid->column('id')->width('10%');
            $grid->column('sort', '排序号(正序)');
            $grid->column('name')->label()->width('10%');
            $grid->column('describe', '描述');
            $grid->column('status', '状态')->switch();

            $grid->addTableClass(['table-text-center']);
            $grid->disableViewButton();
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new BlogCategory(), function (Show $show) {
            $show->field('id');
            $show->field('name');
            $show->field('describe', '描述');
            $show->field('created_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new BlogCategory(), function (Form $form) {
            $form->display('id');
            $form->text('name');
            $form->textarea('describe', '描述');
            $form->text('sort', '排序号');
            $form->switch('status', '状态')
                ->customFormat(function ($v) {
                    return $v == 1 ? 1 : 0;
                })
                ->saving(function ($v) {
                    return $v ? 1 : 0;
                });
            $form->disableViewButton();
        });
    }
}
