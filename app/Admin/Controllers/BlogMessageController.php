<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\BlogMessage;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class BlogMessageController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new BlogMessage(), function (Grid $grid) {
            $grid->model()->orderByDesc('id');
            $grid->column('id');
            $grid->column('name');
            $grid->column('email');
            $grid->column('content');
            $grid->column('created_at', '留言时间');

        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new BlogMessage(), function (Show $show) {
            $show->field('id');
            $show->field('name');
            $show->field('email');
            $show->field('content');
            $show->field('created_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new BlogMessage(), function (Form $form) {
            $form->display('id');
            $form->text('name');
            $form->text('email');
            $form->textarea('content');
        });
    }
}
