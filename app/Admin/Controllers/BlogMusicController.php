<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\BlogMusic;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class BlogMusicController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new BlogMusic(), function (Grid $grid) {
            $grid->column('id');
            $grid->column('name')->display(function ($value){
                return '《'.$value.'》';
            });
            $grid->column('singer')->label();
            $grid->column('cover_image')->image('', 100, 100);
            $grid->column('url')->link(function ($value){
                return config('app.url').'/storage/'.$value;
            });
            $grid->column('created_at');
            $grid->disableViewButton();
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new BlogMusic(), function (Show $show) {
            $show->field('id');
            $show->field('name');
            $show->field('singer');
            $show->field('cover_image');
            $show->field('url');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new BlogMusic(), function (Form $form) {
            $form->display('id');
            $form->text('name');
            $form->text('singer');
            $form->image('cover_image')->autoUpload()->removable(false);
            $form->file('url', '音乐')->autoUpload()->removable(false);
            $form->disableViewButton();
            $form->disableViewCheck();
        });
    }
}
