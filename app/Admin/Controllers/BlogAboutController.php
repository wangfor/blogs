<?php

namespace App\Admin\Controllers;

use App\Admin\Forms\About;
use Dcat\Admin\Layout\Row;
use Dcat\Admin\Layout\Content;
use App\Http\Controllers\Controller;

class BlogAboutController extends Controller
{
    public function index(Content $content)
    {
        return $content->header('关于我')
            ->description('编辑')
            ->body(function (Row $row){
                $about = \App\Models\BlogAbout::query()->first();
                $row->column(12, About::make(['content' => $about->content]));
            });
    }

}
