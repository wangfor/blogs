<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\BlogLife;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class BlogLifeController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new BlogLife(), function (Grid $grid) {
            $grid->model()->orderByDesc('id');
            $grid->column('id');
            $grid->column('content');
            $grid->column('images', '图片')->image('', 100, 100);
            $grid->column('created_at');
            $grid->filter(function (Grid\Filter $filter) {


            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new BlogLife(), function (Show $show) {
            $show->field('id');
            $show->field('content');
            $show->field('images', '图片')->image();
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new BlogLife(), function (Form $form) {
            $form->display('id');
            $form->textarea('content');
            $form->multipleImage('images', '图片')->autoUpload()->sortable()->compress([
                'quality' => 80,
            ])->saving(function ($v) {
                return json_encode($v);
            });
        });
    }
}
