<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\BlogArticle;
use App\Models\BlogCategory;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;
use Dcat\Admin\Widgets\Card;

class BlogArticleController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new BlogArticle(['category']), function (Grid $grid) {
            $grid->model()->orderByDesc('id');
            $grid->column('id');
            $grid->column('category.name', '分类')->label();
            $grid->column('title');
            $grid->column('author');
            $grid->column('brief')->expand(function (Grid\Displayers\Expand $expand) {
                $expand->button('摘要');
                $card = new Card(null, $this->brief);
                return "<div style='padding:10px 10px 0'>$card</div>";
            });
            $grid->column('comment_num');
            $grid->column('read_num');
            $grid->column('created_at');
            $grid->filter(function (Grid\Filter $filter) {
                $filter->panel();
                $filter->equal('category_id', '分类')
                    ->select('api/getCategoryList')
                    ->width(3);
            });

            $grid->disableViewButton();
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new BlogArticle(), function (Show $show) {
            $show->field('id');
            $show->field('category_id');
            $show->field('title');
            $show->field('author');
            $show->field('brief');
            $show->field('content');
            $show->field('comment_num');
            $show->field('read_num');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new BlogArticle(['category']), function (Form $form) {
            $form->display('id');
            $form->select('category_id', '分类')->options('api/getCategoryList');
            $form->text('title');
            $form->text('author');
            $form->textarea('brief');
            $form->markdown('content');
            $form->text('comment_num')->default(0);
            $form->text('read_num')->default(0);
            $form->disableViewButton();
        });
    }
}
