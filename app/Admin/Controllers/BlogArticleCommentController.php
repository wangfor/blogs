<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\BlogArticleComment;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class BlogArticleCommentController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new BlogArticleComment(['article']), function (Grid $grid) {
            $grid->model()->orderByDesc('id');
            $grid->column('id');
            $grid->column('article.title', '文章标题')->label();
            $grid->column('name', '留言人姓名');
            $grid->column('email', '留言人邮箱');
            $grid->column('content');
            $grid->column('created_at', '留言时间');

            $grid->filter(function (Grid\Filter $filter) {
                $filter->panel();
                $filter->equal('article_id', '文章列表')
                    ->select('api/getArticleList')
                    ->width(3);
            });
            $grid->addTableClass(['table-text-center']);
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new BlogArticleComment(), function (Show $show) {
            $show->field('id');
            $show->field('article_id');
            $show->field('name');
            $show->field('email');
            $show->field('content');
            $show->field('created_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new BlogArticleComment(), function (Form $form) {
            $form->display('id');
            $form->select('article_id')->options('api/getArticleList');
            $form->text('name', '留言人姓名');
            $form->text('email', '留言人邮箱');
            $form->textarea('content');
        });
    }
}
