<?php


namespace App\Admin\Controllers\Api;


use App\Models\BlogArticle;
use App\Models\BlogCategory;

class ArticleController
{

    public function getArticleList()
    {
        return BlogArticle::query()->get(['id', 'title as text']);
    }

    public function getCategoryList()
    {
        return BlogCategory::query()->get(['id', 'name as text']);
    }

}
