<?php

namespace App\Admin\Forms;

use App\Models\BlogAbout;
use Dcat\Admin\Contracts\LazyRenderable;
use Dcat\Admin\Traits\LazyWidget;
use Dcat\Admin\Widgets\Form;

class About extends Form implements LazyRenderable
{
    use LazyWidget;

    /**
     * Handle the form request.
     *
     * @param array $input
     *
     * @return mixed
     */
    public function handle(array $input)
    {
        $content = $input['content'];
        BlogAbout::query()->update(['content' => $content]);
        return $this
            ->response()
            ->success('修改成功！')
            ->refresh();

    }

    /**
     * Build a form here.
     */
    public function form()
    {
        $this->markdown('content', '');
        $this->confirm('温馨提示', '您确定要修改吗？');
    }

    /**
     * The data of the form.
     *
     * @return array
     */
    public function default()
    {
        return [
            'content' => $this->payload['content'] ?? '',
        ];
    }
}
