<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Dcat\Admin\Admin;
use App\Admin\Controllers\Api\ArticleController;
use App\Admin\Controllers\HomeController;

Admin::routes();

Route::group([
    'prefix'     => config('admin.route.prefix'),
    'namespace'  => config('admin.route.namespace'),
    'middleware' => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', [HomeController::class, 'index']);
    $router->resource('article', 'BlogArticleController');
    $router->resource('article_comment', 'BlogArticleCommentController');
    $router->resource('category', 'BlogCategoryController');
    $router->resource('message', 'BlogMessageController');
    $router->resource('music', 'BlogMusicController');
    $router->resource('about', 'BlogAboutController');
    $router->resource('life', 'BlogLifeController');


    // API接口
    $router->group(['prefix' => 'api'], function ($router) {
        // 获取文章
        $router->get('getArticleList', [ArticleController::class, 'getArticleList']);
        // 获取分类
        $router->get('getCategoryList', [ArticleController::class, 'getCategoryList']);
    });

});
