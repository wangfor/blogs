# laravel博客

#### 简介
1、框架使用最新的laravel8框架
2、后台使用基于laravel-admin二次开发而成的后台系统构建工具DcatAdmin
高端的博客往往采用最朴素的制作方法

#### 运行环境要求
Nginx 1.8+
PHP 7.3+
Mysql 5.7+
Redis 3.0+


#### 安装教程

1.  克隆源码到本地

```
git clone https://gitee.com/wangfor/blogs.git
```
2.  [Apache/Nginx配置](https://learnku.com/docs/laravel/8.x/installation/9354#e9713a)
3.  安装扩展包依赖

```
composer install
```
4.  生成配置文件

```
cp .env.example .env
```
5.  生成密钥

```
php artisan key:generate
```

管理员账号密码如下:

```
username: admin
password: admin
```



#### 链接入口

```
前端地址：域名
后台地址：域名+admin
```
