<?php

namespace Database\Factories;

use App\Models\BlogArticle;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class BlogArticleFactory extends Factory
{
    /**
     * 工厂对应模型名称
     *
     * @var string
     */
    protected $model = BlogArticle::class;


    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_id' => $this->faker->randomDigitNotNull(),
            'title' => $this->faker->text(50),
            'author' => $this->faker->name,
            'brief' => $this->faker->text(300),
            'content' => '<p>'.$this->faker->paragraphs(10, true).'</p>',
            'comment_num' => 0,
            'read_num' => random_int(50, 100),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
