<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_test_articles', function (Blueprint $table) {
            $table->id();
            $table->integer('category_id')->nullable()->comment('分类ID');
            $table->string('title', 255)->comment('标题');
            $table->string('author', 50)->comment('作者');
            $table->string('brief', 500)->comment('摘要');
            $table->longText('content')->comment('内容');
            $table->integer('comment_num')->default(0)->comment('评论数量');
            $table->integer('read_num')->default(0)->comment('阅读数量');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_articles');
    }
}
