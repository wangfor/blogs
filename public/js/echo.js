var Words;
var TalkWords;
var TalkSub;
var xhr;
var Url;
var Token;


function InputPress() {
    if (event.keyCode == 13) {
        chatRoom();
    }
}

function chatRoom() {
    //定义空字符串
    var str = "";
    if (TalkWords.value == "") {
        // 消息为空时弹窗
        alert("消息不能为空");
        return;
    }
    str = '<div class="btalk"><span>' + TalkWords.value + '</span></div>';

    toChat2(TalkWords.value);

    // 将之前的内容与要发的内容拼接好 提交
    Words.innerHTML = Words.innerHTML + str;
    document.getElementById('talkwords').value = ''
    Words.scrollTop = Words.scrollHeight;
}

function toChat2(query) {
    var url = Url;
    // xhr.onreadystatechange = toPostProcess;
    xhr.open("post", url, true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send("message="+query+"&_token="+Token);
}

function toPostProcess() {/*设置当获XHR对象获取到返回信息后执行以下代码*/
    if (xhr.readyState === 4) {
        if (xhr.status === 200) {
            res = xhr.responseText;
            var obj = eval("(" + res + ")");
            try {
                var textReply = obj.result.content;
                str = '<div class="atalk"><span>' + textReply + '</span></div>';
                // 将之前的内容与要发的内容拼接好 提交
                Words.innerHTML = Words.innerHTML + str;

                // 置空
                TalkWords.value = "";
                // 滑动到最底部
                Words.scrollTop = Words.scrollHeight;
            } catch (e) {
                return handleError(xhr, e);
            }
        } else {
            console.log("可能存在跨域问题");
        }
    }
}

function GetXmlHttpObject() {
    var xmlHttp = null;
    try {
        // Firefox, Opera 8.0+, Safari
        xmlHttp = new XMLHttpRequest();
    } catch (e) {
        // Internet Explorer
        try {
            console.log("Msxml2");
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            console.log("Microsoft");
            xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    return xmlHttp;
}


//is moblie
var sUserAgent = navigator.userAgent.toLowerCase();
var bIsIpad = sUserAgent.match(/ipad/i) == "ipad";
var bIsIphoneOs = sUserAgent.match(/iphone os/i) == "iphone os";
var bIsMidp = sUserAgent.match(/midp/i) == "midp";
var bIsUc7 = sUserAgent.match(/rv:1.2.3.4/i) == "rv:1.2.3.4";
var bIsUc = sUserAgent.match(/ucweb/i) == "ucweb";
var bIsAndroid = sUserAgent.match(/android/i) == "android";
var bIsCE = sUserAgent.match(/windows ce/i) == "windows ce";
var bIsWM = sUserAgent.match(/windows mobile/i) == "windows mobile";

window.onload = function () {
    Words = document.getElementById("words");
    TalkWords = document.getElementById("talkwords");
    TalkSub = document.getElementById("talksub");
    Url = document.getElementById("url").value;
    Token = document.getElementById("token").value;
    xhr = new GetXmlHttpObject();
    TalkSub.onclick = function () {
        chatRoom();
    }
    Words.scrollTop = Words.scrollHeight;
    //is mobile
    if (bIsIpad || bIsIphoneOs || bIsMidp || bIsUc7 || bIsUc || bIsAndroid || bIsCE || bIsWM) {
        console.log("mobile");
        document.getElementById('talk_con_id').className = 'talk_con_mob';
        document.getElementById('words').className = 'talk_show_mob';
        document.getElementById('talk_input_id').className = 'talk_input_mob';
        document.getElementById('talkwords').className = 'talk_word_mob';
    } else {
        console.log("pc");
        document.getElementById('talk_con_id').className = 'talk_con';
        document.getElementById('words').className = 'talk_show';
        document.getElementById('talk_input_id').className = 'talk_input';
        document.getElementById('talkwords').className = 'talk_word';
    }
}
