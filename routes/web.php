<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\IndexController;
use App\Http\Controllers\Web\AboutController;
use App\Http\Controllers\Web\MessageController;
use App\Http\Controllers\Web\LifeController;
use App\Http\Controllers\Web\EchoController;

// 首页
Route::get('/', [IndexController::class, 'index'])->name('index');

// 文章详情
Route::get('article/{id?}', [IndexController::class, 'show'])->name('article');

// 文章评论
Route::post('comment_store', [IndexController::class, 'store'])->name('comment_store');

// 关于我
Route::get('about', [AboutController::class, 'index'])->name('about');

// 留言
Route::get('message', [MessageController::class, 'index'])->name('message');
Route::post('message_store', [MessageController::class, 'store'])->name('message_store');

// 动态
Route::get('life', [LifeController::class, 'index'])->name('life');

// 聊天室
Route::get('echo', [EchoController::class, 'index'])->name('echo');
Route::any('echo_store', [EchoController::class, 'store'])->name('echo_store');
