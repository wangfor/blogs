<?php 
return [
    'labels' => [
        'BlogMessage' => '留言',
        'blog-message' => '留言',
    ],
    'fields' => [
        'name' => '姓名',
        'email' => '邮箱',
        'content' => '内容',
    ],
    'options' => [
    ],
];
