<?php 
return [
    'labels' => [
        'BlogMusic' => '音乐',
        'blog-music' => '音乐',
    ],
    'fields' => [
        'name' => '歌曲',
        'singer' => '歌手',
        'cover_image' => '封面图',
        'url' => '链接',
    ],
    'options' => [
    ],
];
