<?php 
return [
    'labels' => [
        'BlogArticleComment' => '评论',
        'blog-article-comment' => '评论',
    ],
    'fields' => [
        'article_id' => '文章',
        'name' => '姓名',
        'email' => '邮箱',
        'content' => '内容',
    ],
    'options' => [
    ],
];
