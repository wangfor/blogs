<?php 
return [
    'labels' => [
        'BlogArticle' => '文章',
        'blog-article' => '文章',
    ],
    'fields' => [
        'category_id' => '分类ID',
        'title' => '标题',
        'author' => '作者',
        'brief' => '摘要',
        'content' => '内容',
        'comment_num' => '评论数量',
        'read_num' => '阅读数量',
    ],
    'options' => [
    ],
];
