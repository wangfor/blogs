@extends('base')

@section('title', '关于我')

@section('header')
@endsection


@section('content')
    <main class="col-md-12">
        <h1 class="page-title">关于我</h1>
        <article class="post">
            <div class="entry-content clearfix">
{{--                <figure class="img-responsive-center">--}}
{{--                    <img class="img-responsive" src="img/me.jpg" alt="Developer Image">--}}
{{--                </figure>--}}
                {!! $about !!}
                <div class="height-40px"></div>
                <h2 class="title text-center">联系我</h2>
                <ul class="social">
                    <li class="facebook"><a href="/"><span class="ion-social-whatsapp"></span></a></li>
                    <li class="twitter"><a href="/"><span class="ion-social-twitter"></span></a></li>
                    <li class="google-plus"><a href="/"><span class="ion-social-googleplus"></span></a></li>
                    <li class="tumblr"><a href="/"><span class="ion-social-tumblr"></span></a></li>
                </ul>
            </div>
        </article>
    </main>

    <!-- 音乐 -->
    @include('layout.music')
@endsection

