@extends('base')

@section('title', '动态')

@section('header')

@endsection

<link rel="stylesheet" href="{{ asset('css/life.css') }}">
@section('content')

    <div class="life-list" style="margin: 0 10px;width: 100%;">

        @foreach($lives['list'] as $life)
            <section>
                <div class="life-img">
                    <img class="avatar" src="{{ $avatar }}" alt="头像">
                </div>

                <div class="life-head">
                    <div class="text">{{ $life->content }}</div>
                    <div class="images">
                        @foreach($life->images as $image)
                            <img class="image" src="{{ $image }}" alt="图片">
                        @endforeach
                    </div>
                </div>
                <div class="life-bottom">
                    <span class="date">{{ $life->date }}</span>
                </div>
            </section>
        @endforeach
    </div>
    @if($lives['total'] > 0)
        <div class="pagination-simple">
            <a style="@if($lives['page'] === 1) pointer-events: none; @endif"
               href="{{ route('life', ['page' => $lives['page'] - 1]) }}">上一页</a>
            <span class="current">第 {{ $lives['page'] }} 页 / 共 {{ $lives['pages'] }} 页</span>
            <a style="@if($lives['page'] >= $lives['pages']) pointer-events: none; @endif"
               href="{{ route('life', ['page' => $lives['page'] + 1]) }}">下一页</a>
        </div>
    @endif

    <!-- 音乐 -->
    @include('layout.music')
@endsection


