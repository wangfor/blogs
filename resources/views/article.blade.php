@extends('base')

@section('title', $article->title)

@section('header')

@endsection

@section('content')
    <main class="col-md-8">
        <article class="post post-1">
            <header class="entry-header">
                <h1 class="entry-title">{{ $article->title }}</h1>
                <div class="entry-meta">
                    <span class="post-date"><a href="/"><time
                                class="entry-date">{{ date('Y-m-d', strtotime($article->created_at)) }}</time></a></span>
                    <span class="post-author"><a href="#">{{ $article->author }}</a></span>
                    <span class="comments-link"><a href="#">{{ $article->comment_num }} 评论</a></span>
                    <span class="views-count"><a
                            href="#">{{ $article->read_num + $article->real_read_num }} 阅读</a></span>
                </div>
            </header>
            <div class="entry-content clearfix">
                {!! $article->content !!}
            </div>
            <div style="text-align: center;color: #808595;font-size: 14px;line-height: 1.5;margin: 60px auto;
    letter-spacing: .05em;">
                <p>本站部分文章、图片等数据来自互联网，一切版权均归源网站或源作者所有</p>
                <p>如果侵犯了你的权益请来信告知删除。邮箱：1029167904@qq.com</p>
            </div>
        </article>
        <section class="comment-area" id="comment-area">
            <hr>
            <h3>发表评论</h3>
            <form action="{{ route('comment_store') }}" method="post" class="comment-form">
                {{ csrf_field() }}
                <div class="row">
                    <input hidden name="article_id" value="{{ $article->id }}">
                    <div class="col-md-4">
                        <label for="id_name">名字：</label>
                        <input type="text" id="id_name" name="name" required>
                    </div>
                    <div class="col-md-4">
                        <label for="id_email">邮箱：</label>
                        <input type="email" id="id_email" name="email" required>
                    </div>
                    <div class="col-md-12">
                        <label for="id_comment">评论：</label>
                        <textarea name="content" id="id_comment" required></textarea>
                        <button type="submit" class="comment-btn">发表</button>
                    </div>
                </div>
            </form>
            @if(\Illuminate\Support\Facades\Session::has('success'))
                <script>
                    alert("{{\Illuminate\Support\Facades\Session::get('success')}}")
                </script>
            @endif
            <div class="comment-list-panel">
                <h3>评论列表，共 <span>{{ count($comments) }}</span> 条评论</h3>
                <ul class="comment-list list-unstyled">
                    @foreach($comments as $comment)
                        <li class="comment-item">
                            <span class="nickname">{{ $comment->name }}</span>
                            <time class="submit-date">{{ $comment->created_at }}</time>
                            <div class="text">
                                {{ $comment->content }}
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </section>
    </main>
    @include('layout.sidebar')

    <!-- 音乐 -->
    @include('layout.music')
@endsection





