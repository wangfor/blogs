@extends('base')

@section('title', '联系我')

@section('header')
@endsection

@section('content')
    <main class="col-md-12">
        <h1 class="page-title">留言板</h1>
        <article class="post">
            <div class="entry-content clearfix">
                <form action="{{ url('message_store') }}" method="post" class="contact-form">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            {{ csrf_field() }}
                            <input type="text" name="name" placeholder="姓名" required>
                            <input type="email" name="email" placeholder="邮箱" required>
                            <textarea name="content" rows="7" placeholder="您的留言" required></textarea>
                            <button type="submit" class="btn-send btn-5 btn-5b ion-ios-paperplane"><span>给我留言</span>
                            </button>
                        </div>
                    </div>
                </form>
                @if(\Illuminate\Support\Facades\Session::has('success'))
                    <script>
                        alert("{{\Illuminate\Support\Facades\Session::get('success')}}")
                    </script>
                @endif
            </div>
        </article>
    </main>

    <!-- 音乐 -->
    @include('layout.music')
@endsection
