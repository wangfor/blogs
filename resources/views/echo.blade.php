@extends('base')

@section('title', '聊天室')

@section('header')
@endsection

<link rel="stylesheet" href="{{ asset('css/echo.css') }}">
<script src="{{ asset('js/echo.js') }}"></script>
<script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script>
<script src="/js/app.js"></script>
@section('content')

    <div class="talk_con" id="talk_con_id">
        <div class="talk_show" id="words">
            <div class="atalk" style="text-align: center;"><span id="asay">系统：欢迎进入聊天室！</span></div>
        </div>
        <div class="talk_input" id="talk_input_id">
            <input type="text" class="talk_word" id="talkwords" onclick="InputPress()">
            <input type="button" value="发送" class="talk_sub" id="talksub">
        </div>
    </div>
    <input type="text" id="url" value="{{ url('echo_store') }}" hidden>
    <input type="text" id="token" value="{{ csrf_token() }}" hidden>
    <!-- 音乐 -->
    @include('layout.music')
@endsection

<script>
    window.Echo.channel('echo')
        .listen('PublicMessageEvent', (e) => {
            if ("{{ $ip }}" === e.ip){
                str = '<div class="atalk"><span>' + e.message + '</span></div>';
                Words.innerHTML = Words.innerHTML + str;
                TalkWords.value = "";
                Words.scrollTop = Words.scrollHeight;
            }
        });
</script>


