<footer id="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12"  style="border-top: 1px solid #eee;text-align: center;">
                <div class="icp">
                    <a href="https://beian.miit.gov.cn/" target="_blank">
                        <img src="{{ asset('storage/images/20220115/20.png') }}">
                        粤ICP备2021110684号
                    </a>
                </div>
                <p class="copyright">&copy {{date('Y')}} - <a href="" title="" target="_blank"> 箜篌 </a>
                </p>
            </div>
        </div>
    </div>
</footer>
@yield('footer')
