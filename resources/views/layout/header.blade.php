<div class="container">
    <header id="site-header">
        <div class="row">
            <div class="col-md-4 col-sm-5 col-xs-8">
                <div class="logo">
                    <h1><a href="/"><b>Blogs</b> &amp; 箜篌</a></h1>
                </div>
            </div>
            <div class="col-md-8 col-sm-7 col-xs-4">
                <nav class="main-nav" role="navigation">
                    <div class="navbar-header">
                        <button type="button" id="trigger-overlay" class="navbar-toggle">
                            <span class="ion-navicon"></span>
                        </button>
                    </div>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="cl-effect-11"><a href="/" data-hover="首页">首页</a></li>
                            <li class="cl-effect-11"><a href="{{ route('life') }}" data-hover="动态" target="_blank">动态</a></li>
                            <li class="cl-effect-11"><a href="{{ route('about') }}" data-hover="关于">关于</a></li>
                            <li class="cl-effect-11"><a href="{{ route('echo') }}" data-hover="聊天">聊天</a></li>
                            <li class="cl-effect-11"><a href="{{ route('message') }}" data-hover="留言">留言</a></li>
                        </ul>
                    </div>
                </nav>
                <div id="header-search-box">
                    <a id="search-menu" href="#"><span id="search-icon" class="ion-ios-search-strong"></span></a>
                    <div id="search-form" class="search-form">
                        <form role="search" method="get" id="searchform" action="{{ route('index') }}">
                            <input type="search" name="keyword" placeholder="搜索" required>
                            <button type="submit"><span class="ion-ios-search-strong"></span></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </header>
</div>
@yield('header')
