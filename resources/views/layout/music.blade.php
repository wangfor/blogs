<div class="container">
    <div class="row">
        <div class="player">
            <div class="player__header">
                <div class="player__img player__img--absolute slider">
                    <button class="player__button player__button--absolute--nw playlist">
                        <img src="{{ asset('svg/playlist.svg') }}" alt="playlist-icon">
                    </button>
                    <button class="player__button player__button--absolute--center play">
                        <img src="{{ asset('svg/play.svg') }}" alt="play-icon">
                        <img src="{{ asset('svg/pause.svg') }}" alt="pause-icon">
                    </button>
                    <div class="slider__content">
                        @foreach ($musics as $music)
                            <img class="img slider__img" src="{{ $music->new_cover_image }}" alt="cover">
                        @endforeach
                    </div>
                </div>
                <div class="player__controls">
                    <button class="player__button back">
                        <img class="img" src="{{ asset('svg/back.svg') }}" alt="back-icon">
                    </button>
                    <p class="player__context slider__context">
                        <strong class="slider__name"></strong>
                        <span class="player__title slider__title"></span>
                    </p>
                    <button class="player__button next">
                        <img class="img" src="{{ asset('svg/next.svg') }}" alt="next-icon">
                    </button>
                    <div class="progres">
                        <div class="progres__filled"></div>
                    </div>
                </div>
            </div>
            <ul class="player__playlist list">
                @foreach ($musics as $music)
                    <li class="player__song">
                        <img class="player__img img" src="{{ $music->new_cover_image }}" alt="cover">
                        <p class="player__context">
                            <b class="player__song-name">{{ $music->name }}</b>
                            <span class="flex">
                            <span class="player__title">{{ $music->singer }}</span>
                            <span class="player__song-time"></span>
                        </span>
                        </p>
                        <audio class="audio" src="{{ $music->new_url }}"></audio>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@yield('music')

