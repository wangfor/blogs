<aside class="col-md-4">
    <div class="widget widget-recent-posts">
        <h3 class="widget-title">最新文章</h3>
        <ul>
            @foreach ($lastArticles as $lastArticle)
                <li>
                    <a href="{{ route('article', ['id' => $lastArticle->id]) }}" target="_blank">{{ $lastArticle->title }}</a>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="widget widget-archives">
        <h3 class="widget-title">归档</h3>
        <ul>
            @foreach ($archiveDates as $archiveDate)
                <li>
                    <a href="{{ route('index', ['date' => $archiveDate]) }}">{{ $archiveDate }}</a>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="widget widget-category">
        <h3 class="widget-title">分类</h3>
        <ul>
            @foreach ($categories as $category)
                <li>
                    <a href="{{ route('index', ['category_id' => $category->id]) }}">{{ $category->name }}  ({{ count($category->article) }})</a>
                </li>
            @endforeach
        </ul>
    </div>
{{--    <div class="widget widget-tag-cloud">--}}
{{--        <h3 class="widget-title">标签云</h3>--}}
{{--        <ul>--}}
{{--            <li>--}}
{{--                <a href="/">php</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/">java</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="/">go</a>--}}
{{--            </li>--}}
{{--        </ul>--}}
{{--    </div>--}}
{{--    <div class="rss">--}}
{{--        <a href=""><span class="ion-social-rss-outline"></span> RSS 订阅</a>--}}
{{--    </div>--}}
</aside>
@yield('sidebar')
