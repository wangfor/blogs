<div class="overlay overlay-hugeinc">
    <button type="button" class="overlay-close"><span class="ion-ios-close-empty"></span></button>
    <nav>
        <ul>
            <li><a href="/">首页</a></li>
            <li><a href="/">博客</a></li>
            <li><a href="{{ route('about') }}">关于</a></li>
            <li><a href="{{ route('life') }}">动态</a></li>
            <li><a href="{{ route('echo') }}">聊天</a></li>
            <li><a href="{{ route('message') }}">联系</a></li>
        </ul>
    </nav>
</div>
@yield('mobile_menu')
