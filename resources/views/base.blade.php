<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- 标题内容 -->
    <title>@yield('title', '箜篌')</title>
    <!-- css -->
    @include('layout.style')
</head>
<body>
    <!-- 头部内容 -->
    @include('layout.header')

    <!-- 躯干内容 -->
    <div class="content-body">
        <div class="container">
            <div class="row">
                @yield('content')
            </div>
        </div>
    </div>
    <!-- Mobile Menu -->
    @include('layout.mobile_menu')

    <!-- 脚部内容 -->
    @include('layout.footer')

    <!-- js -->
    @include('layout.script')
</body>
</html>
