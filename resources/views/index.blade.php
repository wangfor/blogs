@extends('base')

@section('title', '首页')

@section('header')
@endsection


@section('content')
    {{--  顶部分类内容  --}}
    @if($categoryDetail !== [])
        <div class="category-detail">
            <h1>{{ $categoryDetail->name }}</h1>
            <h4>{{ $categoryDetail->describe }}</h4>
        </div>
    @endif
    <main class="col-md-8">
        @foreach ($articles['list'] as $article)
            <article class="post post-1">
                <header class="entry-header">
                    <h1 class="entry-title">
                        <a href="{{ route('article', ['id' => $article->id]) }}"
                           target="_blank">{{ $article->title }}</a>
                    </h1>
                    <div class="entry-meta">
                        {{--                        <span class="post-category"><a href="#">Django 博客教程</a></span>--}}
                        <span class="post-date"><a href="/"><time class="entry-date"
                                                                  datetime="2012-11-09T23:15:57+00:00">{{ date('Y-m-d', strtotime($article->created_at)) }}</time></a></span>
                        <span class="post-author"><a href="/">{{ $article->author }}</a></span>
                        <span class="comments-link"><a href="/">{{ $article->comment_num }} 评论</a></span>
                        <span class="views-count"><a href="/">{{ $article->read_num + $article->real_read_num }} 阅读</a></span>
                    </div>
                </header>
                <div class="entry-content clearfix">
                    <p>{{ $article->brief }}...</p>
                    <div class="read-more cl-effect-14">
                        <a href="{{ route('article', ['id' => $article->id]) }}" class="more-link">继续阅读 <span
                                class="meta-nav">→</span></a>
                    </div>
                </div>
            </article>
        @endforeach
{{--                <div class="pagination">--}}
{{--                    <ul>--}}
{{--                        <li><a href="">1</a></li>--}}
{{--                        <li><a href="">2</a></li>--}}
{{--                        <li><a href="">3</a></li>--}}
{{--                        <li><a href="">4</a></li>--}}
{{--                        <li><a href="">5</a></li>--}}
{{--                        <li class="current"><a href="">6</a></li>--}}
{{--                        <li><a href="">...</a></li>--}}
{{--                        <li><a href="">51</a></li>--}}
{{--                        <li><a href="">52</a></li>--}}
{{--                        <li><a href="">53</a></li>--}}
{{--                    </ul>--}}
{{--                </div>--}}
        <div class="pagination-simple">
            <a style="@if($articles['page'] === 1) pointer-events: none; @endif"
               href="{{ route('index', ['category_id' => $categoryId, 'page' => $articles['page'] - 1]) }}">上一页</a>
            <span class="current">第 {{ $articles['page'] }} 页 / 共 {{ $articles['pages'] }} 页</span>
            <a style="@if($articles['page'] >= $articles['pages']) pointer-events: none; @endif"
               href="{{ route('index', ['category_id' => $categoryId, 'page' => $articles['page'] + 1]) }}">下一页</a>
        </div>
    </main>
    @include('layout.sidebar')
@endsection




